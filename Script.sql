SELECT prog5db.probleem.probleemid AS ID,prog5db.probleem.beschrijving, 
(SELECT beschrijving FROM prog5db.acties where probleemid = ID order by actieid desc limit 1) AS Oplossing
FROM prog5db.probleem 
left join prog5db.acties 
on prog5db.probleem.probleemid=prog5db.acties.probleemid
where probleem.status = 'opgelost'
group by prog5db.probleem.probleemid;



@media (min-width: 768px) {
  .sidebar-nav .navbar .navbar-collapse {
    padding: 0;
	margin-left: 15px;
    max-height: none;
	width: 40%;
	position: relative;
    top: 100px;
  }
  .sidebar-nav .navbar ul {
    float: none;
  }
  .sidebar-nav .navbar ul:not {
    display: block;
  }
  .sidebar-nav .navbar li {
    float: none;
    display: block;
  }
  .sidebar-nav .navbar li a {
    padding-top: 12px;
    padding-bottom: 12px;
  }
}