<%-- 
    Document   : index
    Created on : 28-mei-2016, 15:41:34
    Author     : Imaginary
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ include file="header.jsp" %>
		
			
        
		<div id="body">
			<div class="col-md-4"><a href="ticketStatus.jsp">
				<span class="tri-icon"><i class="fa fa-ticket fa-4x" aria-hidden="true"></i><br/><br/></span>
				<span class="tri-head">ZOEK OP ID<br/><br/></span>
				<span class="tri-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span>
                        </a></div>
			<div class="col-md-4"><a href="problemenOpzoeken.jsp">
				<span class="tri-icon"><i class="fa fa-cog fa-4x" aria-hidden="true"></i><br/><br/></span>
				<span class="tri-head">OPEN PROBLEMEN<br/><br/></span>
				<span class="tri-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span>
                        </a></div>
			<div class="col-md-4"><a href="geslotenProblemen.jsp">
				<span class="tri-icon"><i class="fa fa-bookmark fa-4x" aria-hidden="true"></i><br/><br/></span>
				<span class="tri-head">OPGELOSTE PROBLEMEN<br/><br/></span>
				<span class="tri-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span>
                        </a></div>
		</div>
        
<%@ include file="footer.jsp" %>

