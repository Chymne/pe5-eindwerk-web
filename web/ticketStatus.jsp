<%-- 
    Document   : ticketStatus
    Created on : 28-mei-2016, 15:42:43
    Author     : Imaginary
--%>

<%@page import="ModelPackage.Problem"%>
<%@page import="ModelPackage.Action"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<%@ include file="header.jsp" %>	
		
		<div id="body">
			
                    <div>
                        
                            <% 
                            String fout = (String)request.getAttribute("foutmelding"); 
                                if (fout != null) {
                                    out.println("<div class=\"alert alert-danger\">"+fout+"</div>");
                                }
                                else {out.println("");}
                            %>
			<p>Geef hieronder uw ticketcode in en druk op de knop. U krijgt de status van uw ticket te zien.</p>
                    </div>
			
                    <div id="formulier">
			<form action="helpdeskServlet" method="POST">
				Ticket ID: <input type="number" name="ticketID" value="" required/>
				<input type="submit" value="Zoek op" class="btn btn-primary" />
			</form>
                    </div>
                    
                    <% Problem probleem = (Problem)request.getAttribute("specifiekprobleem"); %>
                    <br>
                    <table class="table">
                        <tr>
                            <th>
                                <% 
                                if (probleem != null) {
                                    out.println("ProbleemID");
                                }
                                %>
                            </th>
                            <th>
                                <% 
                                if (probleem != null) {
                                    out.println("Probleembeschrijving");
                                }
                                %>
                            </th>
                            <th>
                                <% 
                                if (probleem != null) {
                                    out.println("Status");
                                }
                                %>
                            </th>
                            <th>
                                <% 
                                if (probleem != null) {
                                    out.println("Laatste ondernomen actie");
                                }
                                %>  
                            </th>
                            <th>
                                <% 
                                if (probleem != null) {
                                    out.println("Kostprijs");
                                }
                                %>  
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <% 
                                if (probleem != null) {
                                    out.println(probleem.getID());
                                }
                                %>
                            </td>
                            <td>
                                <% 
                                if (probleem != null) {
                                    out.println(probleem.getDescription());
                                }
                                %>
                            </td>
                            <td>
                                <% 
                                if (probleem != null) {
                                    out.println(probleem.getStatus());
                                }
                                %>
                            </td>
                            <td>
                                <% 
                                if (probleem != null) {
                                    out.println(probleem.getActies().get(0).getDescription());
                                }
                                %>
                            </td>
                            <td>
                                <% 
                                if (probleem != null) {
                                    out.println("€ " + probleem.getActies().get(0).getUnit()*30);
                                }
                                %>
                            </td>
                        </tr>
                    </table>
                    
		</div>
<%@ include file="footer.jsp" %>
