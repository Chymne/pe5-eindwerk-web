<%-- 
    Document   : problemenOpzoeken
    Created on : 28-mei-2016, 18:39:43
    Author     : Imaginary
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="ModelPackage.Problem"%>
<%@page import="UtilPackage.DBConnectie"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ include file="header.jsp" %>	

    <div id="body">
    <table class="table">
        <tr>
            <th>ProbleemID</th>
            <th>Probleembeschrijving</th>
            <th>Status</th>
        </tr>
        
        <%  
            DBConnectie conn = new DBConnectie();
            ArrayList<Problem> alleProblemen = new ArrayList();
            alleProblemen = conn.haalOpenstaandProbleemOp();
        
            for (Problem problem : alleProblemen) {
                out.println("<tr>");
                out.println("<td>"+problem.getID()+"</td><td>"+problem.getDescription()+"</td><td>"+problem.getStatus()+"</td>");
                out.println("</tr>");
            }
        %>
    </table>
    <br>
    </div>

<%@ include file="footer.jsp" %>

