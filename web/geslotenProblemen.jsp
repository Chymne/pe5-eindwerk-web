<%-- 
    Document   : geslotenProblemen
    Created on : 28-mei-2016, 18:48:07
    Author     : Imaginary
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="ModelPackage.Problem"%>
<%@page import="UtilPackage.DBConnectie"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ include file="header.jsp" %>
    
    <div id="body">
    
    <table id ="myTable"  class="display">
        <thead>
            <tr>
                <th>ProbleemID</th>
                <th>Categorie</th>
                <th>Probleembeschrijving</th>
                <th>Oplossing</th>
            </tr>
        </thead>
        <tbody>
            <%
                DBConnectie conn = new DBConnectie();
                ArrayList<Problem> alleGeslotenProblemen = new ArrayList();
                alleGeslotenProblemen = conn.haalGeslotenProblemenOp();
                
                for (Problem problem : alleGeslotenProblemen) {
                out.println("<tr>");
                out.println("<td>"+problem.getID()+"</td><td>"+problem.getCategorie()+"</td><td>"+problem.getDescription()+"</td><td>"+problem.getOplossing()+"</td>");
                out.println("</tr>");
                }
            %>
        </tbody>
    </table>
        
    </div>
<%@ include file="footer.jsp" %>
