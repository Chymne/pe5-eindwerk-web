<%-- 
    Document   : header
    Created on : 28-mei-2016, 15:43:51
    Author     : Imaginary
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    
    <head>
        <title>Helpdesk homepage</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="bootstrap.css">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"/>
		<script src="https://use.fontawesome.com/6b552edd9d.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
		<script>
			$(document).ready(function(){
			// this will get the full URL at the address bar
			var url = window.location.href;

			// passes on every "a" tag 
			$("#navDiv a").each(function() {
            
			// checks if its the same on the address bar
			if(url == (this.href)) { 
            $(this).closest("li").addClass("active");
        }
    });
});
		</script>
		<script>
			$(document).ready(function(){
			$('#myTable').DataTable();
			});
		</script>

    </head>
    
    <body>
        <header>
        
		<div id="navDiv" class="navbar navbar-default" role="navigation">
          <ul class="nav navbar-nav" style="visibility: visible;">
            <li><a href="index.jsp"><i class="fa fa-home" aria-hidden="true"></i>&nbsp&nbsp&nbspHome</a></li>
            <li><a href="ticketStatus.jsp"><i class="fa fa-ticket" aria-hidden="true"></i>&nbsp&nbsp&nbspTicketstatus</a></li>
            <li><a href="problemenOpzoeken.jsp"><i class="fa fa-cog" aria-hidden="true"></i>&nbsp&nbsp&nbspOpen problemen</a></li>
            <li><a href="geslotenProblemen.jsp"><i class="fa fa-bookmark" aria-hidden="true"></i>&nbsp&nbsp&nbspGesloten problemen</a></li>
          </ul>
		</div>
		
		</header>
		<p></p>
                
                <div id="img"><img src="header2.jpg" alt="helpdesk"/></div>
