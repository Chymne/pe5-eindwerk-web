/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UtilPackage;

import ModelPackage.Action;
import ModelPackage.Category;
import ModelPackage.Problem;
import ModelPackage.Technician;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Herlinde Braet
 */

/** 
 *
 * DBConnectie is de database connectie klasse.
 * Indeze klasse wordt de connectie met de database opgebouwd, worden login en passwoord meegegeven en wordt de driver gegenereerd. 
 * Alle mogelijk queries naar de database toe worden ook hier opgeslagen.
 *
 */
public class DBConnectie {
	/**
	*
	* Hier wordt connectie gemaakt met de database. Wachtwoord en username worden meegegeven.
	*
	*/
    private static final String username = "root";
    private static final String password = "";
    private static final String connstring = "jdbc:mysql://127.0.0.1:3306/prog5db";
    private static final String JDBC_DRIVER="com.mysql.jdbc.Driver"; 
    
    ArrayList<Problem> alleProblemen = new ArrayList<>();
    ArrayList <Problem> alleGeslotenProblemen= new ArrayList();
    ArrayList<Action> alleSpecifiekeActies = new ArrayList<>();
    ArrayList<Action> actiesOpProbleem = new ArrayList();
    
    public DBConnectie (){
        Connection conn = null;
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(connstring, username, password);
            System.out.print("Connection ok");
        }
        
        catch (SQLException e){
            System.err.println(e);
        } 
        
        catch (ClassNotFoundException ex) {
            Logger.getLogger(DBConnectie.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Haalt de problemen op die bij een categorie horen. Categorie wordt als id meegestuurd
     * 
     * @param categorieId
     * @return 
     */
    public ArrayList<Problem> haalProblemenVanCategorieOp(Integer categorieId) {
        Connection conn;
        ArrayList<Problem> probleemLijst = new ArrayList();
        ResultSet rs;
        try {
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String select = "SELECT PR.probleemid, PR.beschrijving, PR.categorieid, ST.naam"
                    + " FROM prog5db.probleem PR join prog5db.status ST ON PR.statusid= ST.statusid WHERE categorieid =" + categorieId + ";";
            rs = stmnt.executeQuery(select);
            while (rs.next()) {
                Problem currentProblem = new Problem();
                currentProblem.setID(rs.getInt("probleemid"));
                currentProblem.setDescription(rs.getString("beschrijving"));
                currentProblem.setStatus(rs.getString("ST.naam"));
                probleemLijst.add(currentProblem);
            }
        } catch (SQLException e) {
            System.err.println(e);
        }

        return probleemLijst;

    }
	
    /**
    *
    * Met deze query worden alle problemen met alle bijhorende acties opgehaald
    * @param ID
    */
	
    public void haalProblemenMetActiesOp(int ID) {
        Connection conn = null;
        ResultSet rs; 
        ResultSet rs2; 
        Problem problem = new Problem();
        Action action = new Action();
        
        try {
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String select = "SELECT probleemid, beschrijving FROM prog5db.probleem where probleemid = "+ ID +";";
            
            rs = stmnt.executeQuery(select);
            while ( rs.next() ) {
                problem.setID(rs.getInt("probleemid"));
                problem.setDescription(rs.getString("beschrijving"));
                
                Statement stmnt2 = (Statement) conn.createStatement();
                String select2 = "SELECT * FROM prog5db.acties where probleemid ="+problem.getID()+";";
                
                rs2 = stmnt2.executeQuery(select2);
                while ( rs2.next() ) {
                    action.setID(rs.getInt("actieid"));
                    action.setDescription(rs.getString("beschrijving"));
                    alleSpecifiekeActies.add(action);
                }
                
                problem.setActies(alleSpecifiekeActies);
            } 
        }
        
        catch (SQLException e){
            System.err.println(e);
        }
    }
    
	/**
	*
	* Met deze query worden alle gegevens van een specifiek probleem opgehaald
	*
	*/
	
    public Problem haalSpecifiekProbleemOp(int ID) {
        Connection conn = null;
        ResultSet rs; 
        Problem probleem = new Problem();
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String select = "SELECT probleemid, beschrijving, naam AS status FROM prog5db.probleem left join prog5db.status on prog5db.probleem.statusid = prog5db.status.statusid where probleemid = " +ID+ ";";
            
            rs = stmnt.executeQuery(select);
            while ( rs.next() ) {
                probleem.setID(Integer.parseInt(rs.getString("probleemid"))); 
                probleem.setDescription(rs.getString("beschrijving"));  
                probleem.setStatus(rs.getString("status"));  
            }
            
            rs.close();
            stmnt.close();
            conn.close();
            }
        
        catch (SQLException e){
            System.err.println(e);
        } 
        
        catch (ClassNotFoundException ex) {
            Logger.getLogger(DBConnectie.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return probleem;
    }
    
	/**
	*
	* Met deze query worden alle acties van een bepaald probleem in een arraylist geplaatst
	*
	*/
	
    public ArrayList<Action> haalActiesOp (int ID) {
    
        Connection conn = null;
        ResultSet rs; 
        ArrayList <Action> alleSpecifiekeActies= new ArrayList();
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String select = "SELECT beschrijving FROM prog5db.acties where probleemid = "+ID;
            
            rs = stmnt.executeQuery(select);
            while ( rs.next() ) {
                Action actie = new Action();
                actie.setDescription(rs.getString("beschrijving")); 
                alleSpecifiekeActies.add(actie);
            }     
        } 
        
        catch (ClassNotFoundException ex) {
            Logger.getLogger(DBConnectie.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (SQLException ex) {
            Logger.getLogger(DBConnectie.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return alleSpecifiekeActies;
    }
    
	/**
	*
	* Met deze query wordt een specifieke actie opgehaald
	*
	*/
	
     public Action  haalSpecifiekeActieOp(int ID) {
        Connection conn = null;
        ResultSet rs; 
        Action actie = new Action();
        int units;
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String select = "SELECT beschrijving, SUM(units) AS units FROM prog5db.acties where probleemid = " +ID+ " order by actieid desc limit 1;";
            
            rs = stmnt.executeQuery(select);
            while ( rs.next() ) {
                actie.setDescription(rs.getString("beschrijving")); 
                if (actie.getDescription() == null) actie.setDescription("Nog geen acties");
                try {
                    units = Integer.parseInt(rs.getString("units"));
                }
                catch (NumberFormatException e) {
                    units = 0;
                }
                 
                actie.setUnit(units);
            } 
                    }
        
        catch (SQLException e){
            System.err.println(e);
        } 
        
        catch (ClassNotFoundException ex) {
            Logger.getLogger(DBConnectie.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return actie;
    }
     
	/**
	*
	* Met deze query worden alle openstaande problemen opgehaald
	*
	*/
	 
    public ArrayList<Problem> haalOpenstaandProbleemOp() {
        Connection conn = null;
        ResultSet rs; 
        ArrayList <Problem> alleProblemen= new ArrayList();
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String select = "SELECT probleemid, beschrijving, naam as status FROM prog5db.probleem left join prog5db.status  on prog5db.probleem.statusid = prog5db.status.statusid where prog5db.probleem.statusid <> '3' ;";
            
            rs = stmnt.executeQuery(select);
            while ( rs.next() ) {
                Problem problem = new Problem();
                problem.setID(Integer.parseInt(rs.getString("probleemid"))); 
                problem.setDescription(rs.getString("beschrijving"));  
                problem.setStatus(rs.getString("status")); 
                alleProblemen.add(problem);
            } 
                    }
        
        catch (SQLException e){
            System.err.println(e);
        } 
        
        catch (ClassNotFoundException ex) {
            Logger.getLogger(DBConnectie.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return alleProblemen;
    }
    
	/**
	*
	* Met deze query worden alle gesloten problemen opgehaald
	*
	*/
	
    public ArrayList<Problem> haalGeslotenProblemenOp() {
        Connection conn = null;
        ResultSet rs; 
        ArrayList <Problem> alleGeslotenProblemen= new ArrayList();
        ArrayList<Action> actiesOpProbleem = new ArrayList();
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String select = "SELECT prog5db.probleem.probleemid AS ID, prog5db.categorie.categorienaam, prog5db.probleem.beschrijving,(SELECT beschrijving FROM prog5db.acties where probleemid = ID order by actieid desc limit 1) AS Oplossing FROM prog5db.probleem left join prog5db.acties on prog5db.probleem.probleemid=prog5db.acties.probleemid left join prog5db.categorie on prog5db.probleem.categorieid=prog5db.categorie.categorieid where probleem.statusid = '3'  group by prog5db.probleem.probleemid;";
            
            rs = stmnt.executeQuery(select);
            while ( rs.next() ) {
                Problem problem = new Problem();
                problem.setID(Integer.parseInt(rs.getString("ID"))); 
                problem.setCategorie(rs.getString("categorienaam"));
                problem.setDescription(rs.getString("beschrijving"));
                problem.setOplossing(rs.getString("Oplossing"));
                
//                Action oplossing = new Action();
//                oplossing.setDescription(rs.getString("Oplossing"));
//                actiesOpProbleem.add(oplossing);
//                
//                problem.setActies(actiesOpProbleem); 
                
                alleGeslotenProblemen.add(problem);
            } 
                    }
        
        catch (SQLException e){
            System.err.println(e);
        } 
        
        catch (ClassNotFoundException ex) {
            Logger.getLogger(DBConnectie.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return alleGeslotenProblemen;
    }
    
	/**
	*
	* Met deze query worden alle categorieën opgehaald
	*
	*/
	
    public ArrayList<Category> haalCategorieenOp () {
        Connection conn = null;
        ResultSet rs; 
        ArrayList <Category> alleCats= new ArrayList();
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String select = "";
            
            rs = stmnt.executeQuery(select);
            while ( rs.next() ) {
                Category cat = new Category();
                cat.setDescription(rs.getString("beschrijving"));  
                alleCats.add(cat);
            } 
                    
        }
        catch (SQLException e){
            System.err.println(e);
        } 
        
        catch (ClassNotFoundException ex) {
            Logger.getLogger(DBConnectie.class.getName()).log(Level.SEVERE, null, ex);
        }
        return alleCats;
    }
    
    
    /**
     * Haalt de categorieën op die geen parentcategorie hebben en returnt hiervan een lijst.
     * 
     * @return 
     */
    public ArrayList<Category> getMainCategories() {
        Connection conn;
        ResultSet rs;

        ArrayList<Category> mainCategories = new ArrayList();

        try {
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String select = "SELECT * FROM prog5db.categorie where categorieparent is null;";
            rs = stmnt.executeQuery(select);

            while (rs.next()) {
                Category currentCategory = new Category();
                currentCategory.setID(rs.getInt("categorieid"));
                currentCategory.setDescription(rs.getString("categorienaam"));
                mainCategories.add(currentCategory);
            }
        } catch (SQLException e) {
            System.err.println(e);
        }

        return mainCategories;
    }
    
    /**
     * Haalt alle subcategorieën op van een bepaalde categorie. De categorie wordt meegestuurd
     * als categorieId en het resultaat is een lijst van Category objecten.
     * 
     * @param categorieId
     * @return 
     */
    public ArrayList<Category> getSubCategories(int categorieId) {
        Connection conn;
        ResultSet rs;

        ArrayList<Category> subCategories = new ArrayList();
        try {
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String select = "SELECT * FROM prog5db.categorie where categorieparentid =" + categorieId + ";";
            rs = stmnt.executeQuery(select);

            while (rs.next()) {
                Category currentCategory = new Category();
                currentCategory.setID(rs.getInt("categorieid"));
                currentCategory.setDescription(rs.getString("categorienaam"));
                subCategories.add(currentCategory);
            }
        } catch (SQLException e) {
            System.err.println(e);
        }

        return subCategories;
    }

    /**
     * Haalt een lijst op van alle Category objecten
     * 
     * @return 
     */
    public ArrayList<Category> getAllCategories() {
        Connection conn;
        ResultSet rs;

        ArrayList<Category> mainCategories = new ArrayList();

        try {
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String select = "SELECT * FROM prog5db.categorie;";
            rs = stmnt.executeQuery(select);

            while (rs.next()) {
                Category currentCategory = new Category();
                currentCategory.setID(rs.getInt("categorieid"));
                currentCategory.setDescription(rs.getString("categorienaam"));
                currentCategory.setParentCat(rs.getInt("categorieparentid"));
                mainCategories.add(currentCategory);
            }
        } catch (SQLException e) {
            System.err.println(e);
        }

        return mainCategories;
    }

    /**
     * Haalt de status van een bepaald probleem op.
     * 
     * @param problem
     * @return 
     */
    public int haalStatusIdProbleemOp(Problem problem) {
        Connection conn;
        ResultSet rs;
        int statusId = 1;

        try {
            conn = DriverManager.getConnection(connstring, username, password);
            String selectString = "SELECT * FROM prog5db.status WHERE naam = ?;";
            PreparedStatement selectStatusStmt = conn.prepareStatement(selectString);
            selectStatusStmt.setString(1, problem.getStatus());
            
            
            rs = selectStatusStmt.executeQuery();

            while (rs.next()) {
                statusId = rs.getInt("statusid");
            }

        } catch (Exception e) {
            System.err.println(e);
        }

        return statusId;
    }
    
    /**
     * Haalt een lijst op van alle Technician objecten.
     * 
     * @return 
     */
    public ArrayList<Technician> getAllTechnicians(){
        Connection conn;
        ResultSet rs;
        
        ArrayList<Technician> technicians = new ArrayList();
        
        try {
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String select = "SELECT * FROM prog5db.technieker;";
            rs = stmnt.executeQuery(select);

            while (rs.next()) {
                Technician currentTechnician = new Technician();
                currentTechnician.setID(rs.getInt("techniekerid"));
                currentTechnician.setForname(rs.getString("voornaam"));
                currentTechnician.setSurname(rs.getString("achternaam"));
                technicians.add(currentTechnician);
            }
        } catch (SQLException e) {
            System.err.println(e);
        }

        return technicians;
    }

    /**
     * Voeg een nieuw probleem toe.
     * 
     * @param beschrijving
     * @param categorieid
     * @param statusid 
     */
    public void voegProbleemToe(String beschrijving, int categorieid, int statusid) {
        Connection conn;

        try {
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String insert = "INSERT INTO `prog5db`.`probleem` (`beschrijving`, `categorieid`, `statusid`) VALUES ('" + beschrijving + "', '" + categorieid + "','" + statusid + "')";
            stmnt.executeUpdate(insert);
        } catch (SQLException e) {
            System.err.println(e);
        }
    }

    /**
     * Voeg een nieuw probleem toe.
     * 
     * @param problem
     * @param category 
     */
    public void voegProbleemToe(Problem problem, Category category) {
        Connection conn;

        try {
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String insert = "INSERT INTO `prog5db`.`probleem` (`beschrijving`, `categorieid`, `statusid`) "
                    + "VALUES ('" + problem.getDescription() + "', '" + category.getID() + "','" + haalStatusIdProbleemOp(problem) + "')";
            stmnt.executeUpdate(insert);
        } catch (SQLException e) {
            System.err.println(e);
        }
    }

    /**
     * Pas een bestaand probleem aan met een Problem object.
     * 
     * @param problem 
     */
    public void pasProbleemAan(Problem problem) {
        int problemStatusId = haalStatusIdProbleemOp(problem);
        Connection conn;
        PreparedStatement updateProblemStmt;
        String updateProblemSql = "UPDATE `prog5db`.`probleem` SET `beschrijving`= ?,"
                + "`statusid`= ? WHERE `probleemid`= ?;";
        

        try {
            conn = DriverManager.getConnection(connstring, username, password);
            updateProblemStmt = conn.prepareStatement(updateProblemSql);
            updateProblemStmt.setString(1, problem.getDescription());
            updateProblemStmt.setInt(2, problemStatusId);
            updateProblemStmt.setInt(3, problem.getID());
            updateProblemStmt.executeUpdate();

        } catch (SQLException e) {
            System.err.println(e);
        }

    }

    /**
     * voeg een nieuwe Actie toe.
     * 
     * @param beschrijving
     * @param probleemid
     * @param units
     * @param techniekerid 
     */
    public void voegActieToe(String beschrijving, int probleemid, int units, int techniekerid) {
        Connection conn;

        try {
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String insert = "INSERT INTO `prog5db`.`acties` (`beschrijving`, `probleemid`, `units`, `techniekerid`) VALUES ('" + beschrijving + "', '" + probleemid + "', '" + units + "', '" + techniekerid + "')";
            stmnt.executeUpdate(insert);
        } catch (SQLException e) {
            System.err.println(e);
        }

    }

    /**
     * Voeg een nieuwe technieker toe
     * 
     * @param fname
     * @param lname 
     */
    public void voegTechniekerToe(String fname, String lname) {
        Connection conn;

        try {
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String insert = "INSERT INTO `prog5db`.`technieker` (`voornaam`, `achternaam`) VALUES ('" + fname + "','" + lname + "')";
            stmnt.executeUpdate(insert);
        } catch (SQLException e) {
            System.err.println(e);
        }
    }
    
	/**
	*
	* Deze query telt het aantal rijen in de database om te bepalen hoeveel tickets in de database zitten
	*
	*/
	
    public int telRijen () {
        Connection conn = null;
        ResultSet rs; 
        int teller = 0;
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(connstring, username, password);
            Statement stmnt = (Statement) conn.createStatement();
            String query = "SELECT probleemid FROM prog5db.probleem order by probleemid desc limit 1;";
            
            rs = stmnt.executeQuery(query);
            while ( rs.next() ) {
                teller = Integer.parseInt(rs.getString("probleemid"));
            }
        } 
        
        catch (ClassNotFoundException ex) {
            Logger.getLogger(DBConnectie.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
        catch (SQLException ex) {
            Logger.getLogger(DBConnectie.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return teller;
    }
}
