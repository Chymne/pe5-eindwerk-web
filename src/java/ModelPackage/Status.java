/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prog5Eindwerk.model;

/**
 *
 * @author jan.debonnet
 */
public enum Status {
    /**
     * Status toegekend
     */
    Toegekend(),
    
    /**
     * Status behandeling
     */
    Behandeling(),
    
    /**
     * Status opgelost
     */
    Opgelost();
    
}
