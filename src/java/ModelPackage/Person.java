/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPackage;

/**
 *
 * @author jan.debonnet
 */
public abstract class Person {
    private int ID;
    private String Forname;
    private String Surname;

    /**
     * Constructor
     */
    public Person() {
    }

    /**
     * get ID
     * @return 
     */
    public int getID() {
        return ID;
    }

    /**
     * set ID
     * 
     * @param ID 
     */
    public void setID(int ID) {
        this.ID = ID;
    }
    

    /**
     * get Forename
     * @return 
     */
    public String getForname() {
        return Forname;
    }

    /**
     * set Forname
     * @param Forname 
     */
    public void setForname(String Forname) {
        this.Forname = Forname;
    }

    /**
     * get Surname
     * @return 
     */
    public String getSurname() {
        return Surname;
    }

    /**
     * Set Surname
     * @param Surname 
     */
    public void setSurname(String Surname) {
        this.Surname = Surname;
    }

    @Override
    /**
     * toString method maakt string op basis van voor- en familienaam
     */
    public String toString() {
        return Forname + " " + Surname;
    }
    
    
    
}
