/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPackage;

import java.util.ArrayList;

/**
 *
 * @author Imaginary
 */
public class Problem {
    /**
    *
    * Hieronder staan alle eigenschappen van een probleem.
    */
    private int ID;
    private String description;
    private String status;
    private String categorie;
    private String oplossing;
    
    private ArrayList <Action> acties;

    /**
    *
    * Hieronder staan alle getters en setters.
    *
    */
    
    public String getDescription() {
        return description;
    }
    
    public int getID() {
        return ID;
    }
    
    public String getStatus() {
        return status;
    }
    
    public String getCategorie() {
        return categorie;
    }
    
    public String getOplossing() {
        return oplossing;
    }

    public ArrayList<Action> getActies() {
        return acties;
    }
    
    public void setID(int ID) {
        this.ID = ID;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setActies(ArrayList<Action> acties) {
        this.acties = acties;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public void setOplossing(String oplossing) {
        this.oplossing = oplossing;
    }
}
