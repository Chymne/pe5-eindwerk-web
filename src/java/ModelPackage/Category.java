/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPackage;

/**
 *
 * @author Imaginary
 */

public class Category {
    /**
    *
    * Hieronder staan alle eigenschappen van een categorie.
    */
    private int ID;
    private String description;
    private int parentCat;

    public Category() {
    }

    public Category(int ID, String description, int parentCat) {
        this.ID = ID;
        this.description = description;
        this.parentCat = parentCat;
    }

    /**
    *
    * Hieronder staan alle getters en setters.
    *
    */
    
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getParentCat() {
        return parentCat;
    }

    public void setParentCat(int parentCat) {
        this.parentCat = parentCat;
    }


    
    
    
}

