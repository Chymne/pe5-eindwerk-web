/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPackage;

import ModelPackage.Person;

/**
 *
 * @author jan.debonnet
 */
public class Technician extends Person{

    private double factor;
    
    /**
     * Factor is needed to calculate rates for technician
     * get Factor
     * 
     * @return 
     */
    public double getFactor() {
        return factor;
    }

    /**
     * set Factor
     * @param factor 
     */
    public void setFactor(double factor) {
        this.factor = factor;
    }
    
    
    
}
