/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServletPackage;

import ModelPackage.Action;
import ModelPackage.Problem;
import UtilPackage.DBConnectie;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author Imaginary
 */
@WebServlet("/helpdeskServlet")
public class HelpdeskServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        /**
        *
        * Haal de ID die de gebruiker intikte op
        */
        int id = 0;
        id = Integer.parseInt(request.getParameter("ticketID")); 
        
        /**
        *
        * Maak de database connectie en tel het aantal rijen in de DB
        * Zo weten we direct dat een te groot ticket nooit geldig kan zijn.
        */
        DBConnectie conn = new DBConnectie();
        int teller = conn.telRijen();
        
        if (id <= teller) {
            /**
            *
            * Haal het correcte probleem op uit de database
            */
            Problem probleem = new Problem();
            probleem = conn.haalSpecifiekProbleemOp(id);
            
            /**
            *
            * Haal de bijhorende acties op
            */
            Action actie = new Action();
            actie = conn.haalSpecifiekeActieOp(id);
            
            /**
            *
            * Stop alle acties een Arraylist en koppel het aan het probleem
            */
            ArrayList<Action> laatsteActie = new ArrayList();
            laatsteActie.add(actie);
            probleem.setActies(laatsteActie);
            
            /**
            *
            * Als er geen beschrijving is, dan is er geen probleem en moet er een foutmelding worden getoond.
            */
            if (probleem.getDescription() == null) {
                String fout = "Gelieve een geldig ticket ID op te geven.";
                request.setAttribute("foutmelding", fout);
                request.getSession().setAttribute("foutmelding", fout);
                this.getServletConfig().getServletContext().setAttribute("foutmelding", fout);
                request.getRequestDispatcher("/ticketStatus.jsp").forward(request, response);
            }
            
            /**
            *
            * Is er een probleem, dan moet dit worden doorgegeven aan de JSP pagina.
            */
            request.setAttribute("specifiekprobleem", probleem);
            request.getSession().setAttribute("specifiekprobleem", probleem);
            this.getServletConfig().getServletContext().setAttribute("specifiekprobleem", probleem);
            request.getRequestDispatcher("/ticketStatus.jsp").forward(request, response);
        }
        
        /**
        *
        * Als het ticketnummer groter is dan het grootste ID in de database, dan is het ticket niet correct en moet een foutmelding worden weergegeven.
        */
        else {
            String fout = "Gelieve een geldig ticket ID op te geven.";
            request.setAttribute("foutmelding", fout);
            request.getSession().setAttribute("foutmelding", fout);
            this.getServletConfig().getServletContext().setAttribute("foutmelding", fout);
            request.getRequestDispatcher("/ticketStatus.jsp").forward(request, response);
        }
        
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
